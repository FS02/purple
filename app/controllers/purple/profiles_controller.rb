###
### TODO :
### - ajax pagination for profile listing
### - Handle image size
###

class Purple::ProfilesController < ApplicationController
	## Get 'discover'
	# show all user ordered by follower count
	def discover
		@users = User.order(followers_count: :desc).limit(10)
	end

	## Get 'recomendation'
	# based on mutual relation
	def recomendation
		render html: '<b>Coming soon<b/>'.html_safe
	end

	## Get 'show'
	def show
		@user = User.find_by username: params[:username] #.select("cover", "avatar", "fullname", "username", "description", "location", "website", "follower_count", "following_count")
		@relation = Relation.find_by user: current_user, follow: @user
		@stories = Story.where('user_id' => @user.id).order(created_at: :desc)

		respond_to do |format|
			format.html
		end
	end

	## Get 'edit'
	def edit
		if !user_signed_in?
			redirect_to new_user_session_path
		elsif current_user.username != params[:username]
			redirect_to profile_path(params[:username])
		end
		@user = User.find_by username: params[:username]
	end

	## Patch 'update'
	def update
		@user = User.find_by username: params[:username]

		if edit_profile_params
			params[:user][:avatar] = upload_image(profile_params, "avatar") if params[:user][:avatar]
			params[:user][:cover] = upload_image(profile_params, "cover") if params[:user][:cover]

			@user.update(edit_profile_params)
			redirect_to profile_path(params[:username])
		else
			render 'edit'
		end
	end

	## Get 'followers'
	def followers
		offset = 10 * params[:page].to_i
		@user = User.find_by username: params[:username]
		@users = User.joins('LEFT OUTER JOIN relations ON relations.user_id = users.id').where('relations.follow_id' => @user).select("username", "fullname", "avatar", "cover", "description", "location", "followers_count", "following_count").limit(10).offset(offset)
		@relation = Relation.find_by user: current_user, follow: @user

		respond_to do |format|
			format.html
			format.js { render :file => "users/profile/profile_list.js.erb" }
		end
	end

	## Get 'following'
	def following
		offset = 10 * params[:page].to_i
		@user = User.find_by username: params[:username]
		@users = User.joins('LEFT OUTER JOIN relations ON relations.follow_id = users.id').where('relations.user_id' => @user).select("username", "fullname", "avatar", "cover", "description", "location", "followers_count", "following_count").limit(10).offset(offset)
		@relation = Relation.find_by user: current_user, follow: @user

		respond_to do |format|
			format.html
			format.js { render :file => "users/profile/profile_list.js.erb" }
		end
	end

	## Get 'favorites'
	def favorites
		@user = User.find_by username: params[:username] #.select("cover", "avatar", "fullname", "username", "description", "location", "website", "follower_count", "following_count")
		@relation = Relation.find_by user: current_user, follow: @user
		@stories = Story.where('user_id' => @user.id)

		respond_to do |format|
			format.html
		end
	end

	## Post 'follow'
	def follow
		# Check wether user is exist
		user_to_follow = User.find_by username: params[:username]
		# make sure it's validated and not the current_user profile
		if (user_to_follow and current_user) and (user_to_follow.username != current_user.username)
			# Check wether record exist before create or destroying the relation 
			relation = Relation.find_by user: current_user, follow: user_to_follow
			if (relation)
				if (relation.destroy)
#					current_user.following_count -= 1
#					user_to_follow.follower_count -= 1
#					current_user.save
#					user_to_follow.save
				end
			else
				if (Relation.create user: current_user, follow: user_to_follow)
#					current_user.following_count += 1
#					user_to_follow.follower_count += 1
#					current_user.save
#					user_to_follow.save
				end
			end
		end
		redirect_to profile_path(params[:username])
	end

	private
		## Helper to upload image, image will be replaced if user already upload the same type
		## return the image link
		def upload_image(params, directory)
		  	file_name = params["#{directory}"].original_filename
		   	file_type = file_name.split('.').last
		  
		  	# Create file path
		  	path = File.join("public/" + directory, "#{@user.id}." + file_type)

		  	# Write the file
		  	File.open(path, "wb") { |f| f.write(params["#{directory}"].read) }
		  	return File.join(directory, "#{@user.id}." + file_type)
		end

		## params for edit profile form
		def edit_profile_params
			params.require(:user).permit(:fullname, :description, :location, :website, :avatar, :cover, :theme)
		end
end
