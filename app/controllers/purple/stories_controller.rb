## TODO
# Story
# - Reply

class Purple::StoriesController < ApplicationController
	def index
	end

	def create
		if !user_signed_in?
			redirect_to new_user_session_path
		end


#		params[:story][:attachement] = upload_image(new_story_params, "upload") if params[:story][:attachement]
		
#		p new_story_params
		@story = Story.new(new_story_params)
		@story.user = current_user

		if @story.save
			map_keywords(@story.content)
			
			redirect_to(:back)
		else
			render 'new'
		end
	end

	def new
		if !user_signed_in?
			redirect_to new_user_session_path
		end

		@story = Story.new
	end

	def edit
		@story = Story.find_by id: params[:id]
		if !user_signed_in?
			redirect_to new_user_session_path
		elsif current_user != @story.user
			redirect_to story_path(params[:id])
		end
	end

	def show
		@story = Story.find_by id: params[:id]
	end

	def update
		@story = Story.find_by id: params[:id]
		if !user_signed_in?
			redirect_to new_user_session_path
		elsif current_user != @story.user
			redirect_to story_path(params[:id])
		end

		if (@story.update(update_story_params))
			Keyword.delete_all("story_id = #{@story.id}")
			map_keywords(@story.content)

			redirect_to story_path(@story)
		else
			render 'edit'
		end
	end

	def destroy
		@story = Story.find_by id: params[:id]

		if !user_signed_in?
			redirect_to new_user_session_path
		elsif current_user != @story.user
			redirect_to story_path(params[:id])
		end

		@story.destroy
		redirect_to stories_path
	end

	def repost
		if !user_signed_in?
			redirect_to new_user_session_path
		end

		@story = Story.find_by id: params[:id]
		interaction = Interaction.find_or_initialize_by(user: current_user, owner: @story.user, story: @story)
		interaction.user = current_user
		interaction.owner = @story.user
		interaction.story = @story
		interaction.repost = !interaction.repost

		if (interaction.repost)
			@story.repost_count = @story.repost_count + 1 
		else
			@story.repost_count = @story.repost_count - 1
		end

		interaction.save
		@story.save

		redirect_to(:back)
	end

	def favorite
		if !user_signed_in?
			redirect_to new_user_session_path
		end

		@story = Story.find_by id: params[:id]
		interaction = Interaction.find_or_initialize_by(user: current_user, owner: @story.user, story: @story)
		interaction.user = current_user
		interaction.owner = @story.user
		interaction.story = @story
		interaction.favorite = !interaction.favorite

		if (interaction.favorite)
			@story.favorite_count = @story.favorite_count + 1 
		else
			@story.favorite_count = @story.favorite_count - 1
		end

		interaction.save
		@story.save
			
		redirect_to(:back)
	end

	private
		def new_story_params
			params.require(:story).permit(:content, :attachement, :location, :parent)
		end

		def update_story_params
			params.require(:story).permit(:content, :attachement)
		end

		def map_keywords(story)
			keywords = Hash.new(0)
			@story.content.gsub(/[^0-9A-Za-z # @]/, ' ').split.each do |k| 
				keywords[k] += 1
			end

			records = []
			keywords.each_pair do |k,v|
				records.push(Keyword.new(:story => @story, :value => k, :weight => v))
			end
			Keyword.import records
		end

		## Helper to upload image, image will be replaced if user already upload the same type
		## return the image link
		def upload_image(params, directory)
		  	file_name = params[:attachement].original_filename
		   	file_type = file_name.split('.').last
		  
		  	# Create file path
		  	path = File.join("public/" + directory, "#{Time.now.to_i}." + file_type)

		  	# Write the file
		  	File.open(path, "wb") { |f| f.write(params[:attachement].read) }
		  	return File.join(directory, "#{Time.now.to_i}." + file_type)
		end
end
