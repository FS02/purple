## TODO
# Story
# - Interaction (Repost, Favorite)
# - Ajax (Feed, Interaction)
# Keyword
# - Fix Search
# User
# - Recomendation
# Misc
# - Notifiaction
###

class Purple::DashboardController < ApplicationController
	before_action :authenticate_user!

	def home
		@user = current_user
		@stories = Story.find_by_sql(["SELECT * FROM stories WHERE user_id IN (SELECT follow_id FROM relations WHERE user_id = ?) OR user_id = ? ORDER BY created_at DESC;", current_user.id, current_user.id]);
		@trends = Keyword.find_by_sql(["SELECT value, count(*) as rep FROM keywords WHERE created_at > ? GROUP BY value ORDER BY rep DESC LIMIT 10;", Date.today-1]);
	end

	def interactions
		@user = current_user
		@trends = Keyword.find_by_sql(["SELECT value, count(*) as rep FROM keywords WHERE created_at > ? GROUP BY value ORDER BY rep DESC LIMIT 10;", Date.today-1]);
		@stories = Story.find_by_sql(["SELECT * FROM stories WHERE id in (SELECT story_id FROM keywords WHERE value = ?) ORDER BY created_at DESC;", "@" + current_user.username]);		
	end

	def discover
	end

	def messages
	end

	def search
		## Without join, not strict
		@stories = Story.find_by_sql(["SELECT * FROM stories WHERE id in (SELECT DISTINCT story_id FROM keywords WHERE value in (?)) ORDER BY created_at DESC;", params[:keyword].split]);
		@trends = Keyword.find_by_sql(["SELECT value, count(*) as rep FROM keywords WHERE created_at > ? GROUP BY value ORDER BY rep DESC LIMIT 10;", Date.today-1]);

		#@join = ""
		#params[:keyword].split.each do |p|
		#	@join = "#{@join} INNER JOIN (SELECT story_id FROM keywords WHERE value = #{p}) #{p} ON ("
		#end
		## With join, strict
		#@stories = Story.find_by_sql(["SELECT * FROM stories WHERE id in (SELECT story_id FROM keywords ?) ORDER BY created_at DESC;", @join]);
	end
end
