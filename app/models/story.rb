class Story < ActiveRecord::Base
  belongs_to :user

  has_many :childs,				:class_name => "Story", :foreign_key => 'story_id', counter_cache: :stories_count
  belongs_to :parent,			:class_name => "Story"
end
