class Relation < ActiveRecord::Base
  belongs_to :user,			:class_name => "User", counter_cache: :following_count
  belongs_to :follow,		:class_name => "User", counter_cache: :followers_count
end