# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# auto create link from plain text, https://github.com/bryanwoods/autolink-js/blob/master/autolink.coffee
autoLink = (options...) ->
  pattern = ///
    (^|[\s\n]|<br\/?>) # Capture the beginning of string or line or leading whitespace
    (
      (?:https?|ftp):// # Look for a valid URL protocol (non-captured)
      [\-A-Z0-9+\u0026\u2019@#/%?=()~_|!:,.;]* # Valid URL characters (any number of times)
      [\-A-Z0-9+\u0026@#/%=~()_|] # String must end in a valid URL character
    )
  ///gi

  return @replace(pattern, "$1<a href='$2'>$2</a>") unless options.length > 0

  option = options[0]
  linkAttributes = (
    " #{k}='#{v}'" for k, v of option when k isnt 'callback'
  ).join('')

  @replace pattern, (match, space, url) ->
    link = option.callback?(url) or
      "<a href='#{url}'#{linkAttributes}>#{url}</a>"

    "#{space}#{link}"

String.prototype['autoLink'] = autoLink


# Update the timer every span when id is timer
applyAutolink = ->
	for elm in document.querySelectorAll("p")
  		do (elm) -> elm.innerHTML = elm.innerHTML.autoLink({ target: "_blank", rel: "nofollow", id: "1" })

# Get elapsed time since
@timeSince = (timeStamp) ->
	now = new Date()
	past = new Date(timeStamp)
	secondsPast = (now.getTime() - past.getTime()) / 1000
	if (secondsPast < 10)
		"Just now"
	else if (secondsPast < 60)
		"#{Math.floor(secondsPast)} s"
	else if (secondsPast < 3600)
		"#{Math.floor(secondsPast/60)} m"
	else if (secondsPast <= 86400)
		"#{Math.floor(secondsPast/3600)} h"
	else
		day = past.getDate()
#		month = past.toDateString().match(/[a-zA-Z]*/)[0].replace(" ", "")
		month = past.toDateString().split(" ")[1]
		year = if past.getFullYear() is now.getFullYear() then "" else past.getFullYear()
		"#{day} #{month} #{year}"

# Update the timer every span when id is timer
updateTimer = ->
	for elm in document.querySelectorAll("#timer")
  		do (elm) -> elm.innerText = timeSince(parseFloat(elm.getAttribute ('timeStamp')))

ready = ->
	updateTimer()
	applyAutolink()
	# Update elapsed time every 10s
	setInterval () ->
		updateTimer()
	, 10000

readyTurbo = ->
	updateTimer()
	applyAutolink()
	
$(document).ready(ready)
$(document).on('page:load', readyTurbo)