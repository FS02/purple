class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.references :story, index: true
      t.string :value
      t.integer :weight

      t.timestamps null: false
    end
    add_foreign_key :keywords, :stories
  end
end
