class CreateInteractions < ActiveRecord::Migration
  def change
    create_table :interactions do |t|
      t.references :user, index: true
      t.references :owner, index: true
      t.references :story, index: true
      t.boolean :repost
      t.boolean :favorite

      t.timestamps null: false
    end
    add_foreign_key :interactions, :users
    add_foreign_key :interactions, :owners
    add_foreign_key :interactions, :stories
  end
end
