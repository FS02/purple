class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.string :content
      t.string :attachment
      t.string :location
      t.integer :reply_count, default: 0
      t.integer :favorite_count, default: 0
      t.integer :repost_count, default: 0
      t.references :user, index: true, null: false
      t.references :parent, index: true

      t.timestamps null: false
    end
    add_foreign_key :stories, :users
    add_foreign_key :stories, :parents
  end
end
