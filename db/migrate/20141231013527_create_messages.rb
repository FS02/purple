class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :sender, index: true
      t.references :receiver, index: true
      t.boolean :sender_read
      t.boolean :receiver_read
      t.string :content
      t.string :attachement

      t.timestamps null: false
    end
    add_foreign_key :messages, :senders
    add_foreign_key :messages, :receivers
  end
end
