class CreateRelations < ActiveRecord::Migration
  def change
    create_table :relations do |t|
      t.references :user, index: true, null: false
      t.references :follow, index: true, null: false

      t.timestamps null: false
    end
    add_foreign_key :relations, :users
    add_foreign_key :relations, :follows
  end
end
