# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141231013527) do

  create_table "interactions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "owner_id"
    t.integer  "story_id"
    t.boolean  "repost"
    t.boolean  "favorite"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "interactions", ["owner_id"], name: "index_interactions_on_owner_id"
  add_index "interactions", ["story_id"], name: "index_interactions_on_story_id"
  add_index "interactions", ["user_id"], name: "index_interactions_on_user_id"

  create_table "keywords", force: :cascade do |t|
    t.integer  "story_id"
    t.string   "value"
    t.integer  "weight"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "keywords", ["story_id"], name: "index_keywords_on_story_id"

  create_table "messages", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.boolean  "sender_read"
    t.boolean  "receiver_read"
    t.string   "content"
    t.string   "attachement"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "messages", ["receiver_id"], name: "index_messages_on_receiver_id"
  add_index "messages", ["sender_id"], name: "index_messages_on_sender_id"

  create_table "relations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "follow_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "relations", ["follow_id"], name: "index_relations_on_follow_id"
  add_index "relations", ["user_id"], name: "index_relations_on_user_id"

  create_table "stories", force: :cascade do |t|
    t.string   "content"
    t.string   "attachment"
    t.string   "location"
    t.integer  "reply_count"
    t.integer  "favorite_count"
    t.integer  "repost_count"
    t.integer  "user_id"
    t.integer  "parent_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "stories", ["parent_id"], name: "index_stories_on_parent_id"
  add_index "stories", ["user_id"], name: "index_stories_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                    default: "",                   null: false
    t.string   "encrypted_password",       default: "",                   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            default: 0,                    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",          default: 0,                    null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "username",                 default: "",                   null: false
    t.string   "fullname",                 default: "",                   null: false
    t.string   "description"
    t.string   "location"
    t.string   "website"
    t.string   "language"
    t.string   "avatar",                   default: "avatar/default.png"
    t.string   "cover",                    default: "cover/default.png"
    t.string   "theme"
    t.integer  "stories_count",            default: 0
    t.integer  "favorite_count",           default: 0
    t.integer  "followers_count",          default: 0
    t.integer  "following_count",          default: 0
    t.integer  "unreadnotification_count", default: 0
    t.integer  "unreadmessage_count",      default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  add_index "users", ["username"], name: "index_users_on_username", unique: true

end
